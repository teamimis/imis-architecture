<?php
session_start();

$errors = array();
$notices = array();
$successes = array();

$users = array(
    'elderly' => array('password' => 'test', 'type' => 'carereceiver'),
    'relative' => array('password' => 'test', 'type' => 'relative'),
    'doctor' => array('password' => 'test', 'type' => 'caretaker'),
    'nurse' => array('password' => 'test', 'type' => 'caretaker'),
);

if(!empty($_POST['submit'])) {
    switch($_POST['submit']) {
    case 'Log in':
        $username = $_POST['username'];
        $password = $_POST['password'];

        if(in_array($username, array_keys($users))) {
            $user = $users[$username];
            if($password = $user['password']) {
                $_SESSION['imis_logged_in'] = true;
                $_SESSION['user'] = $username;
                $_SESSION['type'] = $user['type'];

                header('Location: ?p=start');
            }
        }

        $errors[] = 'Wrong credentials.';
        break;
    }
}

if(!empty($_GET['a'])) {
    switch($_GET['a']) {
    case 'logout':
        $_SESSION['imis_logged_in'] = false;
        header('Location: ?p=start');
        break;
    }
}

$LOGGED_IN = false;

if(!empty($_SESSION['imis_logged_in'])) {
    $LOGGED_IN = true;
    $USER = $_SESSION['user'];
    $USER_GROUP = $_SESSION['type'];
}

$pages = array(
    'start' => array('name' => 'Home'),
    'community' => array('name' => 'Community'),
    'calendar' => array('name' => 'Calendar'),
    'tools' => array('name' => 'Tools'),
    'login' => array('name' => 'Log in'),
);
$pages_public = array('start', '404', 'login');

$p = 'start';

if(!empty($_GET['p'])) {
    $p = '404';
    if(in_array($_GET['p'], array_keys($pages))) {
        $p = $_GET['p'];
    }

    if(!$LOGGED_IN && !in_array($p, $pages_public)) {
        $p = 'login';
    }
}

$p_path = 'pages/' . $p . '.php';
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>IMIS - Integrated Mobile Information System</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body class="page-<?php echo $p; ?>">
        <div id="container">
            <div id="header">
                <div id="logo">
                    <img src="images/logo.gif" alt="IMIS" />
                    <span>Integrated Mobile Information System</span>
                </div>
                <div id="menu">
                    <ul>
<?php
foreach($pages as $page => $pagemeta) {
    if(($LOGGED_IN || in_array($page, $pages_public))) {
        if($page == 'login' && $LOGGED_IN) { } else {
            echo '<li' . ($page == $p ? ' class="active"' : '') . '><a href="?p=' . $page . '">' . $pagemeta['name'] . '</a></li>';
        }
    }
}
if($LOGGED_IN) {
    echo '<li><a href="?a=logout">Log out</a></li>';
}
?>
                    </ul>
                    <div style="clear: both;"></div> 
                </div>
            </div>
            <div id="main">
<?php
foreach($errors as $error) {
    echo '<div class="msg error">' . $error . '</div>';
}
foreach($notices as $notice) {
    echo '<div class="msg notice">' . $notice . '</div>';
}
foreach($successes as $success) {
    echo '<div class="msg success">' . $success . '</div>';
}
?>
<?php
if(is_readable($p_path)) {
    include($p_path);
}
?>
            </div>
        </div>
    </body>
</html>
