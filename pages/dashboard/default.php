<div class="box">
    <p>Integrated Mobile Information System (IMIS) for Diabetic Homecare is a project financed by VINNOVA. IMIS is to integrate both healthcare providers (such as hospital care, primary care, home care, and school care) and healthcare receivers (especially chronic, diabetic, and elderly care) into a web based and mobile platform in order to increase healthcare interoperability, integrity, mobility for both sides:</p>
    <p>First (patient in focus), the IMIS is to provide the diabetic patients with a mobile-network communication platform for homecare supervision, self-treatment, preparation before face-to-face diagnoses.</p>
    <p>Second (care provider in focus), the IMIS is to provide all care providers (doctors, nurses, relatives, etc.) with the same mobile-network communication platform as the patients to access and share the same and right information on right time for a seamless co-operative work among organizations and among persons.</p>
</div>
