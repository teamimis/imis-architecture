<?php
if($LOGGED_IN) {
    switch($USER_GROUP) {
    case 'caretaker':
        include('dashboard/caretaker.php');
        break;
    case 'carereceiver':
        include('pages/dashboard/carereceiver.php');
        break;
    case 'relative':
        include('pages/dashboard/relative.php');
        break;
    default:
        include('pages/dashboard/default.php');
        break;
    }
} else {
    include('pages/dashboard/default.php');
}
?>
