<div class="msg notice">You can log in with the usernames "elderly", "relative" and "doctor" with the password "test" to see different dashboards.</div>
<h1>You need to log in to access the site</h1>
<div class="box">
    <form action="" method="post">
        <label for="username">ID Number</label><input type="text" name="username" /><br />
        <label for="password">PIN Code</label><input type="password" name="password" /><br />
        <input type="submit" name="submit" value="Log in" />
    </form>
</div>
